# SMASHDADON

Smash together all the things I want in a client into one static webpage.

Run locally with nix-shell

## TODO
- seperate html,css,js for rendering threads
- read post id from url param
- render entire thread like it's a reddit comment section
- use /api/v1/statuses/:id/context to find thread "root", recursively render all descendants
- multiple TL system, navbar on the top to select TL
- account pages, pretty much just that account's tl but with info before all the posts
- logging in and the several problems that causes
