{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
	nativeBuildInputs = with pkgs; [ ran ];
	shellHook = "ran -nc; exit";
}
