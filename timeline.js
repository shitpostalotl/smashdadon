function apiGet(serverUrl,type,arg){
	const http = new XMLHttpRequest();
	const types = {
		"timeline": "timelines/public",
		"post": "statuses/"
	};
	http.open("GET", `https://${serverUrl}/api/v1/${types[type]+arg}`, false);
	http.send();
	return JSON.parse(http.responseText);
}

function renderPost(postObject, parent, reverse, debug){
	postlog = "--- RENDERING POST --";
	if (parent == null){
		containerElement = ( reverse ? document.getElementById("timeline").insertBefore(document.createElement("div"),document.getElementById("timeline").children[0]) : document.getElementById("timeline").appendChild(document.createElement("div")));
		containerElement.classList.add("tlpost");
	}

	postElement = (parent == null) ? containerElement.appendChild(document.createElement("div")) : document.getElementById(parent).parentNode.insertBefore(document.createElement("div"), document.getElementById(parent));
	postElement.classList.add((parent == null) ? "mainpost" : "parent"); 
	postElement.id = postObject.id;
	postlog += (`\nId: ${postObject.id+((parent == null) ? "" : "\nParent: "+parent)}`);

	const postDate = new Date(postObject.created_at);
	try {
		const test = postObject.account.avatar_static;
	} catch (error) {
		postElement.innerHTML = "ERROR";
		return;
	}

	const specificElementTypes = {"info": `<img src=${postObject.account.avatar_static}><div><a href='https://${cookies['instance']}/@${postObject.account.acct}'><pre class='username'>${postObject.account.display_name}</pre></a><a href='https://${cookies['instance']}/@${postObject.account.acct}'><pre class='servername'>@${postObject.account.acct}</pre></a></div><div><a href='https://${cookies['instance']}/@${postObject.account.acct}/${postObject.id}'>${new Intl.DateTimeFormat('en-US', {day:'numeric',month:'long',year:'numeric'}).format(postDate)}</a><a href='https://${cookies['instance']}/@${postObject.account.acct}/${postObject.id}'>${new Intl.DateTimeFormat('en-US', {hour:'numeric',minute:'numeric'}).format(postDate)}</a></div>`, "content": `<div>${postObject.content}</div>`, "interact": `<div>${postObject.replies_count}↩️</div><div>${postObject.reblogs_count}🔁</div><div>${postObject.favourites_count}💟</div><div>🏷️</div>`};
	for ([k,v] of Object.entries(specificElementTypes)){
		specificElement = postElement.appendChild(document.createElement((k == "content" && postObject.sensitive) ? "details" : "div"))
		specificElement.classList.add(k);
		postlog += `\nRendering ${k}: ${v}`;
		
		for (i in postObject.emojis){
			postlog += `\nRendering emoji: ${postObject.emojis[i].shortcode}`;
			v = v.replace(`:${postObject.emojis[i].shortcode}:`, `<img src="${postObject.emojis[i].static_url}" alt="${postObject.emojis[i].shortcode}" class="emoji">`);
		}

		postlog += ((k == "content" && postObject.sensitive) ? `\nPost sensitive as ${postObject.spoiler_text}` : "");
		specificElement.innerHTML = ((k == "content" && postObject.sensitive) ? `<summary>${postObject.spoiler_text}</summary>${v}` : v);
	}

	mediaContainer = postElement.children[1].appendChild(document.createElement("div"));
	mediaContainer.classList.add("mediaContainer");
	for (media in postObject.media_attachments){
		postlog += `\nRendering media: ${postObject.media_attachments[media].url}`;
		mediaObject = postObject.media_attachments[media];
		mediaContainer.innerHTML += (mediaObject.type == "video") ? `<video controls><source src=${mediaObject.url}>${mediaObject.description}</video>` : `<a href=${mediaObject.remote_url} target="_blank"><img class="media" src=${mediaObject.preview_url} title="${(mediaObject.description == null) ? "" : mediaObject.description}" ></a>`;
	}

	if (postObject.in_reply_to_id != null){
		postlog += `\nRendering parent: ${postObject.in_reply_to_id}`;
		renderPost(apiGet(cookies["instance"],"post",postObject.in_reply_to_id), postObject.id)
	}

	if (debug){
		console.log(postlog);
		console.log(postObject);
	}
}

function renderTimeline(mode){
	try {
		timelineObject = apiGet(cookies["instance"],"timeline", `?${(mode=="older") ? "max_id" : "min_id" }=${document.querySelector("#timeline").children[(((mode=="older")) ? document.querySelector("#timeline").children["length"]-1 : 0)].id}`)
		previous = document.getElementById("timeline").children[( mode == "newer") ? 0 : document.getElementById("timeline").children.length-1];
	} catch (error) {
		timelineObject = apiGet(cookies["instance"],"timeline", "")
		previous = document.getElementById("timeline")
	}
	const postIds = [...document.querySelectorAll('#timeline > div > .mainpost')].map(({ id }) => id);
	for (post in timelineObject){
		if(!postIds.includes(timelineObject[post].id)){
			renderPost(timelineObject[post], null, (mode=="newer"), true);
		}
	}
	previous.scrollIntoView();
}

const cookies = Object.fromEntries(document.cookie.split('; ').map(v=>v.split(/=(.*)/s).map(decodeURIComponent)))
